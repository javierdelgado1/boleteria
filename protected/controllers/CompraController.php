<?php

class CompraController extends GxController {
	public function filters() {
		return array(
				'accessControl', 
				);
	}

	public function accessRules() {
		return array(
				array('allow',
					'actions'=>array( 'create',  'view', 'pdf'),
					'users'=>array('*'),
					),
				array('allow', 
					'actions'=>array('minicreate','update', 'index'),
					'users'=>array(Yii::app()->user->id),
					),
				array('allow', 
					'actions'=>array('admin','delete'),
					'users'=>array(Yii::app()->user->id),
					),
				array('deny', 
					'users'=>array('*'),
					),
				);
	}

	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Compra'),
		));
	}
	public function actionPDF($id){
		# mPDF
        $mPDF1 = Yii::app()->ePdf->mpdf();
 
        # You can easily override default constructor's params
        $mPDF1 = Yii::app()->ePdf->mpdf('', 'A5');
 
        # render (full page)
        $mPDF1->WriteHTML($this->render('viewpdf', array(
				'model' => $this->loadModel($id, 'Compra'),
			), true)
		);
        //$mPDF1->WriteHTML($this->render('view', array(), true));
 
        # Load a stylesheet
        //$stylesheet = file_get_contents('..'.Yii::app()->request->baseUrl . '/css/pdf.css');
        //$mPDF1->WriteHTML($stylesheet, 1);
 
        # renderPartial (only 'view' of current controller)
        //$mPDF1->WriteHTML($this->renderPartial('index', array(), true));
 
        # Renders image
       //$mPDF1->WriteHTML(CHtml::image(Yii::getPathOfAlias('webroot.css') . '/bg.gif' ));
 
        # Outputs ready PDF
        $mPDF1->Output();
	}

	public function actionCreate() {
		$model = new Compra;

		$this->performAjaxValidation($model, 'compra-form');

		if (isset($_POST['Compra'])) {
			$model->setAttributes($_POST['Compra']);
			if($this->validarMaxBoletos($model))
				Yii::app()->user->setFlash('pasajeros','La cantidad de pasajeros de este boleto supera la cantidad máxima del vuelo');
			else{
				if($this->validarMaxBoletosTotales($model))
					Yii::app()->user->setFlash('DemasiadosPasajeros','No hay suficientes cupos libres en este vuelo para cubrir la cantidad propocionada');
				else
					if ($model->save()) {
						if (Yii::app()->getRequest()->getIsAjaxRequest())
							Yii::app()->end();
						else
							$this->redirect(array('view', 'id' => $model->idcompra));
					}
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	function validarMaxBoletos($model = null){
		$vuelo = Vuelos::model()->findByPk($model->numerovuelo);
		if($model->cantidaddeadultos + $model->cantidaddeninos > $vuelo->maxboleto)
			return true;
		return false;
	}

	function validarMaxBoletosTotales($model = null){
		$compras = Compra::model()->findAll("numerovuelo=$model->numerovuelo");
		$pasajeros = $model->cantidaddeadultos + $model->cantidaddeninos;
		$vuelo = Vuelos::model()->findByPk($model->numerovuelo);
		foreach ($compras as $value) {
            $pasajeros += $value->cantidaddeadultos + $value->cantidaddeninos;
        }
        echo $pasajeros;
		if($pasajeros > $vuelo->maxboleto)
			return true;
		return false;
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Compra');

		$this->performAjaxValidation($model, 'compra-form');

		if (isset($_POST['Compra'])) {
			$model->setAttributes($_POST['Compra']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->idcompra));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Compra')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'La solicitud es inválida.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Compra');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new Compra('search');
		$model->unsetAttributes();

		if (isset($_GET['Compra']))
			$model->setAttributes($_GET['Compra']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}