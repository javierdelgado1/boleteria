<?php

class OrigenesController extends GxController {

public function filters() {
	return array(
			'accessControl', 
			);
}

public function accessRules() {
	return array(
			array('allow', 
				'actions'=>array('index', 'view'),
				'users'=>array(Yii::app()->user->id),
				),
			array('allow', 
				'actions'=>array('minicreate', 'create', 'update', 'admin', 'delete'),
				'users'=>array(Yii::app()->user->id),
				),
			array('deny', 
				'users'=>array('*'),
				),
			);
}

	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Origenes'),
		));
	}

	public function actionCreate() {
		$model = new Origenes;

		$this->performAjaxValidation($model, 'origenes-form');

		if (isset($_POST['Origenes'])) {
			$model->setAttributes($_POST['Origenes']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->idorigen));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Origenes');

		$this->performAjaxValidation($model, 'origenes-form');

		if (isset($_POST['Origenes'])) {
			$model->setAttributes($_POST['Origenes']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->idorigen));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Origenes')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Su solicitud es inválida.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Origenes');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new Origenes('search');
		$model->unsetAttributes();

		if (isset($_GET['Origenes']))
			$model->setAttributes($_GET['Origenes']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}