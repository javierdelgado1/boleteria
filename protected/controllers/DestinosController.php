<?php

class DestinosController extends GxController {

public function filters() {
	return array(
			'accessControl', 
			);
}

public function accessRules() {
	return array(
			array('allow', 
				'actions'=>array('index', 'view'),
				'users'=>array(Yii::app()->user->id),
				),
			array('allow', 
				'actions'=>array('minicreate', 'create', 'update', 'admin', 'delete'),
				'users'=>array(Yii::app()->user->id),
				),
			array('deny', 
				'users'=>array('*'),
				),
			);
}

	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Destinos'),
		));
	}

	public function actionCreate() {
		$model = new Destinos;

		$this->performAjaxValidation($model, 'destinos-form');

		if (isset($_POST['Destinos'])) {
			$model->setAttributes($_POST['Destinos']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->iddestino));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Destinos');

		$this->performAjaxValidation($model, 'destinos-form');

		if (isset($_POST['Destinos'])) {
			$model->setAttributes($_POST['Destinos']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->iddestino));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Destinos')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Su solicitud es inválida.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Destinos');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new Destinos('search');
		$model->unsetAttributes();

		if (isset($_GET['Destinos']))
			$model->setAttributes($_GET['Destinos']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}