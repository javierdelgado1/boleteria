<?php

class VistaController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}

	public function filters()
	{
	 return array(
	  'accessControl', // perform access control for CRUD operations
	  'postOnly + delete', // we only allow deletion via POST request
	 );
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}