<?php

class AdministradoresController extends GxController {

public function filters() {
	return array(
			'accessControl', 
			);
}

public function accessRules() {
	return array(
			array('allow', 
				'actions'=>array('index', 'view'),
				'users'=>array(Yii::app()->user->id),
				),
			array('allow', 
				'actions'=>array('minicreate', 'create', 'update', 'admin', 'delete'),
				'users'=>array(Yii::app()->user->id),
				),
			array('deny', 
				'users'=>array('*'),
				),
			);
}

	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Administradores'),
		));
	}

	public function actionCreate() {
		$model = new Administradores;

		$this->performAjaxValidation($model, 'administradores-form');

		if (isset($_POST['Administradores'])) {
			$model->setAttributes($_POST['Administradores']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->idadministrador));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Administradores');

		$this->performAjaxValidation($model, 'administradores-form');

		if (isset($_POST['Administradores'])) {
			$model->setAttributes($_POST['Administradores']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->idadministrador));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Administradores')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Administradores');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new Administradores('search');
		$model->unsetAttributes();

		if (isset($_GET['Administradores']))
			$model->setAttributes($_GET['Administradores']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}