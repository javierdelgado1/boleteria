<?php

class AerolineasController extends GxController {

public function filters() {
	return array(
			'accessControl', 
			);
}

public function accessRules() {
	return array(
			array('allow',
				'actions'=>array('index','view'),
				'users'=>array(Yii::app()->user->id),
				),
			array('allow', 
				'actions'=>array('minicreate', 'create','update'),
				'users'=>array(Yii::app()->user->id),
				),
			array('allow', 
				'actions'=>array('admin','delete'),
				'users'=>array(Yii::app()->user->id),
				),
			array('deny', 
				'users'=>array('*'),
				),
			);
}

	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Aerolineas'),
		));
	}

	public function actionCreate() {
		$model = new Aerolineas;

		$this->performAjaxValidation($model, 'aerolineas-form');


		if (isset($_POST['Aerolineas'])) {
			$model->setAttributes($_POST['Aerolineas']);
			try{
						if ($model->save()) {
							if (Yii::app()->getRequest()->getIsAjaxRequest())
								Yii::app()->end();
							else
								$this->redirect(array('view', 'id' => $model->idaerolinea));
						}

				}
				catch (Exception $e) {
				    Yii::app()->user->setFlash('contact','El nombre o el rif ya existen');
				}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Aerolineas');

		$this->performAjaxValidation($model, 'aerolineas-form');

		if (isset($_POST['Aerolineas'])) {
			$model->setAttributes($_POST['Aerolineas']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->idaerolinea));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Aerolineas')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Aerolineas');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new Aerolineas('search');
		$model->unsetAttributes();

		if (isset($_GET['Aerolineas']))
			$model->setAttributes($_GET['Aerolineas']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}