<?php

class ClasesController extends GxController {

public function filters() {
	return array(
			'accessControl', 
			);
}

public function accessRules() {
	return array(
			array('allow', 
				'actions'=>array('index', 'view'),
				'users'=>array('@'),
				),
			array('allow', 
				'actions'=>array('minicreate', 'create', 'update', 'admin', 'delete'),
				'users'=>array(Yii::app()->user->id),
				),
			array('deny', 
				'users'=>array('*'),
				),
			);
}

	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Clases'),
		));
	}

	public function actionCreate() {
		$model = new Clases;

		$this->performAjaxValidation($model, 'clases-form');

		if (isset($_POST['Clases'])) {
			$model->setAttributes($_POST['Clases']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->idclase));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Clases');

		$this->performAjaxValidation($model, 'clases-form');

		if (isset($_POST['Clases'])) {
			$model->setAttributes($_POST['Clases']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->idclase));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Clases')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Su solicitud es inválida.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Clases');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new Clases('search');
		$model->unsetAttributes();

		if (isset($_GET['Clases']))
			$model->setAttributes($_GET['Clases']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}