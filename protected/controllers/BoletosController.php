<?php

class BoletosController extends GxController {

public function filters() {
	return array(
			'accessControl', 
			);
}

public function accessRules() {
	return array(
			array('allow', 
				'actions'=>array('index', 'view'),
				'users'=>array(Yii::app()->user->id),
				),
			array('allow', 
				'actions'=>array('minicreate', 'create', 'update', 'admin', 'delete'),
				'users'=>array(Yii::app()->user->id),
				),
			array('deny', 
				'users'=>array('*'),
				),
			);
}

	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Boletos'),
		));
	}

	public function actionCreate() {
		$model = new Boletos;

		$this->performAjaxValidation($model, 'boletos-form');

		if (isset($_POST['Boletos'])) {
			$model->setAttributes($_POST['Boletos']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->idboleto));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Boletos');

		$this->performAjaxValidation($model, 'boletos-form');

		if (isset($_POST['Boletos'])) {
			$model->setAttributes($_POST['Boletos']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->idboleto));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Boletos')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Su solicitud es inválida.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Boletos');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new Boletos('search');
		$model->unsetAttributes();

		if (isset($_GET['Boletos']))
			$model->setAttributes($_GET['Boletos']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}