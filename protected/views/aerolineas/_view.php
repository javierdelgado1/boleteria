<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('Id Aerolinea')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->idaerolinea), array('view', 'id' => $data->idaerolinea)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('Nombre')); ?>:
	<?php echo GxHtml::encode($data->nombre); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('Rif')); ?>:
	<?php echo GxHtml::encode($data->rif); ?>
	<br />

</div>