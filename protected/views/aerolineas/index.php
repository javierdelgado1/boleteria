<?php

$this->breadcrumbs = array(
	Aerolineas::label(2),
	Yii::t('app', 'Index'),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'Crear') . ' ' . Aerolineas::label(), 'url' => array('create')),
	array('label'=>Yii::t('app', 'Gestionar') . ' ' . Aerolineas::label(2), 'url' => array('admin')),
);
?>

<h1><?php echo GxHtml::encode(Aerolineas::label(2)); ?></h1>

<?php
$dataProvider=new CActiveDataProvider('aerolineas');
 
 $this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider'=>$dataProvider,
    'columns'=>array(
	'idaerolinea',
	'nombre',
	'rif',      
       
    ),
 ));
?>