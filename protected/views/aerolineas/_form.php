<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'aerolineas-form',
	'enableAjaxValidation' => true,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Campos con'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'son requeridos'); ?>.
		<?php if(Yii::app()->user->hasFlash('contact')): ?> 
		<div class="flash-success alert alert-error">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		    <?php echo Yii::app()->user->getFlash('contact'); ?>
		</div>
		<?php endif; ?>
	</p>

	<?php echo $form->errorSummary($model); ?>
<div style="padding-left:30px;">
		<div class="row">
		<?php echo $form->labelEx($model,'Nombre'); ?>
		<?php echo $form->textField($model, 'nombre', array('maxlength' => 25)); ?>
		<?php echo $form->error($model,'nombre'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'Rif'); ?>
		<?php echo $form->textField($model, 'rif', array('maxlength' => 25)); ?>
		<?php echo $form->error($model,'rif'); ?>
		</div><!-- row -->
</div>
		
<?php
echo GxHtml::submitButton(Yii::t('app', 'Agregar Aerolinea'));
$this->endWidget();
?>
</div><!-- form -->