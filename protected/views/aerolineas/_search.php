<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>
<div style="padding-left:30px;">
	<div class="row">
		<?php echo $form->label($model, 'idaerolinea'); ?>
		<?php echo $form->textField($model, 'idaerolinea'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'nombre'); ?>
		<?php echo $form->textField($model, 'nombre', array('maxlength' => 25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'rif'); ?>
		<?php echo $form->textField($model, 'rif', array('maxlength' => 25)); ?>
	</div>

	<div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Buscar')); ?>
	</div>
</div>
<?php $this->endWidget(); ?>

</div><!-- search-form -->
