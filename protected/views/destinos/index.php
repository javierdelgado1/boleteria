<?php

$this->breadcrumbs = array(
	Destinos::label(2),
	Yii::t('app', 'Index'),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'Crear') . ' ' . Destinos::label(), 'url' => array('create')),
	array('label'=>Yii::t('app', 'Gestionar') . ' ' . Destinos::label(2), 'url' => array('admin')),
);
?>

<h1><?php echo GxHtml::encode(Destinos::label(2)); ?></h1>

<?php
$dataProvider=new CActiveDataProvider('destinos');
 
 $this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider'=>$dataProvider,
    'columns'=>array(
	'iddestino',
	'nombre',     
       
    ),
 ));
?>