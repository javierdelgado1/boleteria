<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('iddestino')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->iddestino), array('view', 'id' => $data->iddestino)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('nombre')); ?>:
	<?php echo GxHtml::encode($data->nombre); ?>
	<br />

</div>