<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'destinos-form',
	'enableAjaxValidation' => true,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Campos con'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'son requeridos'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>
<div style="padding-left:30px;">
		<div class="row">
		<?php echo $form->labelEx($model,'nombre'); ?>
		<?php echo $form->textField($model, 'nombre', array('maxlength' => 25)); ?>
		<?php echo $form->error($model,'nombre'); ?>
		</div><!-- row -->
</div>
		<!-- <label><?php echo GxHtml::encode($model->getRelationLabel('vueloses')); ?></label>
		<?php echo $form->checkBoxList($model, 'vueloses', GxHtml::encodeEx(GxHtml::listDataEx(Vuelos::model()->findAllAttributes(null, true)), false, true)); ?>
 -->
<?php
echo GxHtml::submitButton(Yii::t('app', 'Guardar'));
$this->endWidget();
?>
</div><!-- form -->