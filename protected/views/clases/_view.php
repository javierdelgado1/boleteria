<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('idclase')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->idclase), array('view', 'id' => $data->idclase)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('nombre')); ?>:
	<?php echo GxHtml::encode($data->nombre); ?>
	<br />

</div>