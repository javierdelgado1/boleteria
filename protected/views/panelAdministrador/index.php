<?php
/* @var $this PanelAdministradorController */

$this->breadcrumbs=array(
	'Panel Administrador',
);
?>






<div class="row-fluid">
  <div class="span4"><?php echo CHtml::link('<center><a class="btn btn-large" href="index.php?r=aerolineas"><i class="icon-globe"></i></a>Panel AeroLineas</center>', Yii::app()->request->baseUrl .'/index.php?r=aerolineas') ?></div>
  <div class="span4"><?php echo CHtml::link('<center><a class="btn btn-large" href="index.php?r=origenes"><i class="icon-home"></i></a>Panel Origenes</center>', Yii::app()->request->baseUrl .'/index.php?r=origenes') ?></div>
  <div class="span4"><?php echo CHtml::link('<center><a class="btn btn-large" href="index.php?r=destinos"><i class="icon-share-alt"></i></a>Panel Destinos</center>', Yii::app()->request->baseUrl .'/index.php?r=destinos') ?></div>
 
</div>
<br>
<br>
<div class="row-fluid">
 <div class="span4"><?php echo CHtml::link('<center><a class="btn btn-large" href="index.php?r=vuelos"><i class="icon-plus"></i></a>Panel Vuelos</center>', Yii::app()->request->baseUrl .'/index.php?r=vuelos') ?></div>
  <div class="span4"><?php echo CHtml::link('<center><a class="btn btn-large" href="index.php?r=compra"><i class="icon-wrench"></i></a>Panel Viajes</center>', Yii::app()->request->baseUrl .'/index.php?r=compra') ?></div>
  <div class="span4"><?php echo CHtml::link('<center><a class="btn btn-large" href="index.php?r=administradores"><i class="icon-user"></i></a>Panel Administradores</center>', Yii::app()->request->baseUrl .'/index.php?r=administradores') ?></div>
</div>


