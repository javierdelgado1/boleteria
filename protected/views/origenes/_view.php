<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('idorigen')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->idorigen), array('view', 'id' => $data->idorigen)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('nombre')); ?>:
	<?php echo GxHtml::encode($data->nombre); ?>
	<br />

</div>