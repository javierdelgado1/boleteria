<?php

$this->breadcrumbs = array(
	Origenes::label(2),
	Yii::t('app', 'Index'),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'Crear') . ' ' . Origenes::label(), 'url' => array('create')),
	array('label'=>Yii::t('app', 'Gestionar') . ' ' . Origenes::label(2), 'url' => array('admin')),
);
?>

<h1><?php echo GxHtml::encode(Origenes::label(2)); ?></h1>

<?php
$dataProvider=new CActiveDataProvider('origenes'); 
 $this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider'=>$dataProvider,
    'columns'=>array(
	'idorigen',
	'nombre', 
       
    ),
 ));
?>
