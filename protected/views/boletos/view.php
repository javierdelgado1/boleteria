<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);

$this->menu=array(
	array('label'=>Yii::t('app', 'Lista') . ' ' . $model->label(2), 'url'=>array('index')),
	array('label'=>Yii::t('app', 'Crear') . ' ' . $model->label(), 'url'=>array('create')),
	array('label'=>Yii::t('app', 'Modificar') . ' ' . $model->label(), 'url'=>array('update', 'id' => $model->idboleto)),
	array('label'=>Yii::t('app', 'Eliminar') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->idboleto), 'confirm'=>'¿Está seguro que desea eliminar este elemento?')),
	array('label'=>Yii::t('app', 'Gestionar') . ' ' . $model->label(2), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
'idboleto',
array(
			'name' => 'idvuelo0',
			'type' => 'raw',
			'value' => $model->idvuelo0 !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->idvuelo0)), array('vuelos/view', 'id' => GxActiveRecord::extractPkValue($model->idvuelo0, true))) : null,
			),
array(
			'name' => 'idpasajero0',
			'type' => 'raw',
			'value' => $model->idpasajero0 !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->idpasajero0)), array('pasajeros/view', 'id' => GxActiveRecord::extractPkValue($model->idpasajero0, true))) : null,
			),
'fecha',
'estado:boolean',
	),
)); ?>

