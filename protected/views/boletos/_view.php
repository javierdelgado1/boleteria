<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('idboleto')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->idboleto), array('view', 'id' => $data->idboleto)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('idvuelo')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->idvuelo0)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('idpasajero')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->idpasajero0)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('fecha')); ?>:
	<?php echo GxHtml::encode($data->fecha); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('estado')); ?>:
	<?php echo GxHtml::encode($data->estado); ?>
	<br />

</div>