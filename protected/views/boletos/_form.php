<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'boletos-form',
	'enableAjaxValidation' => true,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Campos con'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'Son requeridos'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>
	<div style="padding-left:30px;">
		<div class="row">
		<?php echo $form->labelEx($model,'idvuelo'); ?>
		<?php echo $form->dropDownList($model, 'idvuelo', GxHtml::listDataEx(Vuelos::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'idvuelo'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'idpasajero'); ?>
		<?php echo $form->dropDownList($model, 'idpasajero', GxHtml::listDataEx(Pasajeros::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'idpasajero'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'fecha'); ?>
		<?php $form->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model' => $model,
			'attribute' => 'fecha',
			'value' => $model->fecha,
			'options' => array(
				'showButtonPanel' => true,
				'changeYear' => true,
				'dateFormat' => 'yy-mm-dd',
				),
			));
; ?>
		<?php echo $form->error($model,'fecha'); ?>
		</div><!-- row -->

		<div class="row">
		<?php echo $form->labelEx($model,'estado'); ?>
		<?php echo $form->checkBox($model, 'estado'); ?>
		<?php echo $form->error($model,'estado'); ?>
		</div><!-- row -->
	</div>

<?php
echo GxHtml::submitButton(Yii::t('app', 'Guardar'));
$this->endWidget();
?>
</div><!-- form -->