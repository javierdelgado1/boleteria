<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>
<div style="padding-left:30px;">
	<div class="row">
		<?php echo $form->label($model, 'idvuelo'); ?>
		<?php echo $form->textField($model, 'idvuelo'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'idaerolinea'); ?>
		<?php echo $form->dropDownList($model, 'idaerolinea', GxHtml::listDataEx(Aerolineas::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'hora'); ?>
		<?php echo $form->textField($model, 'hora'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'idorigen'); ?>
		<?php echo $form->dropDownList($model, 'idorigen', GxHtml::listDataEx(Origenes::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'iddestino'); ?>
		<?php echo $form->dropDownList($model, 'iddestino', GxHtml::listDataEx(Destinos::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'idclase'); ?>
		<?php echo $form->dropDownList($model, 'idclase', GxHtml::listDataEx(Clases::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>
	<div class="row">
		<?php echo $form->label($model, 'costo'); ?>
		<?php echo $form->textField($model, 'costo'); ?>
	</div>
	<div class="row">
		<?php echo $form->label($model, 'maxboleto'); ?>
		<?php echo $form->textField($model, 'maxboleto'); ?>
	</div>

	<div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Buscar')); ?>
	</div>
</div>

<?php $this->endWidget(); ?>
</div>
</div><!-- search-form -->
