<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'vuelos-form',
	'enableAjaxValidation' => true,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Campos con'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'son requeridos'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>
<div style="padding-left:30px;">
		<div class="row">
		<?php echo $form->labelEx($model,'idaerolinea'); ?>
		<?php echo $form->dropDownList($model, 'idaerolinea', GxHtml::listDataEx(Aerolineas::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'idaerolinea'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'hora'); ?>
		<?php echo $form->textField($model, 'hora'); ?>
		<?php echo $form->error($model,'hora'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'idorigen'); ?>
		<?php echo $form->dropDownList($model, 'idorigen', GxHtml::listDataEx(Origenes::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'idorigen'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'iddestino'); ?>
		<?php echo $form->dropDownList($model, 'iddestino', GxHtml::listDataEx(Destinos::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'iddestino'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'idclase'); ?>
		<?php echo $form->dropDownList($model, 'idclase', GxHtml::listDataEx(Clases::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'idclase'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'costo'); ?>
		<?php echo $form->textField($model, 'costo'); ?>
		<?php echo $form->error($model,'costo'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'maxboleto'); ?>
		<?php echo $form->textField($model, 'maxboleto'); ?>
		<?php echo $form->error($model,'maxboleto'); ?>
		</div><!-- row -->
</div>
		
<?php
echo GxHtml::submitButton(Yii::t('app', 'Guardar'));
$this->endWidget();
?>
</div>
</div><!-- form -->