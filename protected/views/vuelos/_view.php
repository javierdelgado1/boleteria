<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('idvuelo')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->idvuelo), array('view', 'id' => $data->idvuelo)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('idaerolinea')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->idaerolinea0)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('hora')); ?>:
	<?php echo GxHtml::encode($data->hora); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('idorigen')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->idorigen0)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('iddestino')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->iddestino0)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('idclase')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->idclase0)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('costo')); ?>:
	<?php echo GxHtml::encode($data->costo); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('maxboleto')); ?>:
	<?php echo GxHtml::encode($data->maxboleto); ?>
	<br />

</div>