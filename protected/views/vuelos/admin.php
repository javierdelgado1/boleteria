<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	Yii::t('app', 'Gestionar'),
);

$this->menu = array(
		array('label'=>Yii::t('app', 'Lista') . ' ' . $model->label(2), 'url'=>array('index')),
		array('label'=>Yii::t('app', 'Crear') . ' ' . $model->label(), 'url'=>array('create')),
	);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('vuelos-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('app', 'Gestionar') . ' ' . GxHtml::encode($model->label(2)); ?></h1>

<p>
También puede escribir un operador de comparación(&lt;, &lt;=, &gt;, &gt;=, &lt;&gt; or =) o al principio de cada uno de los valores de búsqueda para especificar cómo se debe hacer la comparación..
</p>

<?php echo GxHtml::link(Yii::t('app', 'Advanced Search'), '#', array('class' => 'search-button')); ?>
<div class="search-form">
<?php $this->renderPartial('_search', array(
	'model' => $model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'vuelos-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		'idvuelo',
		array(
				'name'=>'idaerolinea',
				'value'=>'GxHtml::valueEx($data->idaerolinea0)',
				'filter'=>GxHtml::listDataEx(Aerolineas::model()->findAllAttributes(null, true)),
				),
		'hora',
		array(
				'name'=>'idorigen',
				'value'=>'GxHtml::valueEx($data->idorigen0)',
				'filter'=>GxHtml::listDataEx(Origenes::model()->findAllAttributes(null, true)),
				),
		array(
				'name'=>'iddestino',
				'value'=>'GxHtml::valueEx($data->iddestino0)',
				'filter'=>GxHtml::listDataEx(Destinos::model()->findAllAttributes(null, true)),
				),
		array(
				'name'=>'idclase',
				'value'=>'GxHtml::valueEx($data->idclase0)',
				'filter'=>GxHtml::listDataEx(Clases::model()->findAllAttributes(null, true)),
				),
		/*
		'maxboleto',
		*/
		array(
			'class' => 'CButtonColumn',
		),
	),
)); ?>