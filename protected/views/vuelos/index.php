<?php

$this->breadcrumbs = array(
	Vuelos::label(2),
	Yii::t('app', 'Index'),
);

$this->menu = array(
    array('label'=>Yii::t('app', 'Crea') . ' ' . Compra::label(), 'url' => array('create')),
    array('label'=>Yii::t('app', 'Gestionar') . ' ' . Compra::label(2), 'url' => array('admin')),
);
?>

<h1><?php echo GxHtml::encode(Vuelos::label(2)); ?></h1>


<?php 
/*$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
));*/
 $this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider'=>$dataProvider,
    'columns'=>array(
        'idvuelo',
        'idaerolinea0',
        'idorigen0',
        'iddestino0',
        'hora',
        'idclase0',
        'costo',
    ),
 ));
?>