<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);

$this->menu=array(
	array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('index')),
	array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
	array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('update', 'id' => $model->idvuelo)),
	array('label'=>Yii::t('app', 'Delete') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->idvuelo), 'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
'idvuelo',
array(
			'name' => 'idaerolinea0',
			'type' => 'raw',
			'value' => $model->idaerolinea0 !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->idaerolinea0)), array('aerolineas/view', 'id' => GxActiveRecord::extractPkValue($model->idaerolinea0, true))) : null,
			),
'hora',
array(
			'name' => 'idorigen0',
			'type' => 'raw',
			'value' => $model->idorigen0 !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->idorigen0)), array('origenes/view', 'id' => GxActiveRecord::extractPkValue($model->idorigen0, true))) : null,
			),
array(
			'name' => 'iddestino0',
			'type' => 'raw',
			'value' => $model->iddestino0 !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->iddestino0)), array('destinos/view', 'id' => GxActiveRecord::extractPkValue($model->iddestino0, true))) : null,
			),
array(
			'name' => 'idclase0',
			'type' => 'raw',
			'value' => $model->idclase0 !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->idclase0)), array('clases/view', 'id' => GxActiveRecord::extractPkValue($model->idclase0, true))) : null,
			),
'maxboleto',
	),
)); ?>

<h2><?php echo GxHtml::encode($model->getRelationLabel('boletoses')); ?></h2>
<?php
	echo GxHtml::openTag('ul');
	foreach($model->boletoses as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('boletos/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');
?>