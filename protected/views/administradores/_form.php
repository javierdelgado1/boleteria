<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'administradores-form',
	'enableAjaxValidation' => true,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Campos con'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'son requeridos'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row2">
		<?php echo $form->labelEx($model,'usuario'); ?>
		<?php echo $form->textField($model, 'usuario', array('maxlength' => 25)); ?>
		<?php echo $form->error($model,'usuario'); ?>
		</div><!-- row -->
		<div class="row2">
		<?php echo $form->labelEx($model,'contrasena'); ?>
		<?php echo $form->textField($model, 'contrasena', array('maxlength' => 25)); ?>
		<?php echo $form->error($model,'contrasena'); ?>
		</div><!-- row -->


<?php
echo GxHtml::submitButton(Yii::t('app', 'Guardar'));
$this->endWidget();
?>
</div><!-- form -->