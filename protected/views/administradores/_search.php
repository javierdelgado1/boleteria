<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>
<div style="padding-left:30px;">
	<div class="row">
		<?php echo $form->label($model, 'idadministrador'); ?>
		<?php echo $form->textField($model, 'idadministrador'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'usuario'); ?>
		<?php echo $form->textField($model, 'usuario', array('maxlength' => 25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'contrasena'); ?>
		<?php echo $form->textField($model, 'contrasena', array('maxlength' => 25)); ?>
	</div>

	<div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Buscar')); ?>
	</div>
</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
