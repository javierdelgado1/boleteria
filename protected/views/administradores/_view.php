<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('idadministrador')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->idadministrador), array('view', 'id' => $data->idadministrador)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('usuario')); ?>:
	<?php echo GxHtml::encode($data->usuario); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('contrasena')); ?>:
	<?php echo GxHtml::encode($data->contrasena); ?>
	<br />

</div>