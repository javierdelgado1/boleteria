<?php

$this->breadcrumbs = array(
	Administradores::label(2),
	Yii::t('app', 'Index'),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'Crear') . ' ' . Administradores::label(), 'url' => array('create')),
	array('label'=>Yii::t('app', 'Gestionar') . ' ' . Administradores::label(2), 'url' => array('admin')),
);
?>

<h1><?php echo GxHtml::encode(Administradores::label(2)); ?></h1>

<?php
$dataProvider=new CActiveDataProvider('administradores');
 
 $this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider'=>$dataProvider,
    'columns'=>array(
	'idadministrador',
	'usuario',
	'contrasena',  
       
    ),
 ));
?>