<?php

$this->breadcrumbs = array(
	Compra::label(2),
	Yii::t('app', 'Index'),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'Crea') . ' ' . Compra::label(), 'url' => array('create')),
	array('label'=>Yii::t('app', 'Gestionar') . ' ' . Compra::label(2), 'url' => array('admin')),
);
?>

<h1><?php echo GxHtml::encode(Compra::label(2)); ?></h1>

-<?php
$dataProvider=new CActiveDataProvider('compra');
 
 $this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider'=>$dataProvider,
    'columns'=>array(
        'nombre',
        'apellido',
        'pasaporte',
        
        
       
    ),
 ));
?>