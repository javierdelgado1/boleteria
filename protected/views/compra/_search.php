<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>
<div style="padding-left:30px;">
	<div class="row">
		<?php echo $form->label($model, 'idcompra'); ?>
		<?php echo $form->textField($model, 'idcompra'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'nombre'); ?>
		<?php echo $form->textField($model, 'nombre', array('maxlength' => 25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'apellido'); ?>
		<?php echo $form->textField($model, 'apellido', array('maxlength' => 25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'pasaporte'); ?>
		<?php echo $form->textField($model, 'pasaporte', array('maxlength' => 25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'nacionalidad'); ?>
		<?php echo $form->textField($model, 'nacionalidad', array('maxlength' => 25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'cantidaddeadultos'); ?>
		<?php echo $form->textField($model, 'cantidaddeadultos'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'cantidaddeninos'); ?>
		<?php echo $form->textField($model, 'cantidaddeninos'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'cantidaddeescalas'); ?>
		<?php echo $form->dropDownList($model, 'cantidaddeescalas', array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'numerovuelo'); ?>
		<?php echo $form->dropDownList($model, 'numerovuelo', GxHtml::listDataEx(Vuelos::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Buscar')); ?>
	</div>
</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
