<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('idcompra')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->idcompra), array('view', 'id' => $data->idcompra)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('nombre')); ?>:
	<?php echo GxHtml::encode($data->nombre); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('apellido')); ?>:
	<?php echo GxHtml::encode($data->apellido); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('pasaporte')); ?>:
	<?php echo GxHtml::encode($data->pasaporte); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('nacionalidad')); ?>:
	<?php echo GxHtml::encode($data->nacionalidad); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('cantidaddeadultos')); ?>:
	<?php echo GxHtml::encode($data->cantidaddeadultos); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('cantidaddeninos')); ?>:
	<?php echo GxHtml::encode($data->cantidaddeninos); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('cantidaddeescalas')); ?>:
	<?php echo GxHtml::encode($data->cantidaddeescalas); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('numerovuelo')); ?>:
	<?php echo GxHtml::encode(GxHtml::valueEx($data->numerovuelo0)); ?>
	<br />
	*/ ?>
</div>