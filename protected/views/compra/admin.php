<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	Yii::t('app', 'Gestionar'),
);

$this->menu = array(
		array('label'=>Yii::t('app', 'Lista') . ' ' . $model->label(2), 'url'=>array('index')),
		array('label'=>Yii::t('app', 'Crear') . ' ' . $model->label(), 'url'=>array('create')),
	);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('compra-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('app', 'Gestionar') . ' ' . GxHtml::encode($model->label(2)); ?></h1>

<p>
También puede escribir un operador de comparación(&lt;, &lt;=, &gt;, &gt;=, &lt;&gt; or =) o al principio de cada uno de los valores de búsqueda para especificar cómo se debe hacer la comparación..
</p>

<?php echo GxHtml::link(Yii::t('app', 'Busqueda avanzada'), '#', array('class' => 'search-button')); ?>
<div class="search-form">
<?php $this->renderPartial('_search', array(
	'model' => $model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'compra-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		'idcompra',
		'nombre',
		'apellido',
		'pasaporte',
		'nacionalidad',
		'cantidaddeadultos',
		/*
		'cantidaddeninos',
		'cantidaddeescalas',
		array(
				'name'=>'numerovuelo',
				'value'=>'GxHtml::valueEx($data->numerovuelo0)',
				'filter'=>GxHtml::listDataEx(Vuelos::model()->findAllAttributes(null, true)),
				),
		*/
		array(
			'class' => 'CButtonColumn',
		),
	),
)); ?>