<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'compra-form',
	'enableAjaxValidation' => true,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Campos con'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'son requeridos'); ?>.
		<?php if(Yii::app()->user->hasFlash('pasajeros')): ?> 
		<div class="flash-success alert alert-error">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		    <?php echo Yii::app()->user->getFlash('pasajeros'); ?>
		</div>
		<?php endif; ?>
		<?php if(Yii::app()->user->hasFlash('DemasiadosPasajeros')): ?> 
		<div class="flash-success alert alert-error">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		    <?php echo Yii::app()->user->getFlash('DemasiadosPasajeros'); ?>
		</div>
		<?php endif; ?>
	</p>

	<?php echo $form->errorSummary($model); ?>
<div style="padding-left:30px;">
		<div class="row">
		<?php echo $form->labelEx($model,'nombre'); ?>
		<?php echo $form->textField($model, 'nombre', array('maxlength' => 25)); ?>
		<?php echo $form->error($model,'nombre'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'apellido'); ?>
		<?php echo $form->textField($model, 'apellido', array('maxlength' => 25)); ?>
		<?php echo $form->error($model,'apellido'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'pasaporte'); ?>
		<?php echo $form->textField($model, 'pasaporte', array('maxlength' => 25)); ?>
		<?php echo $form->error($model,'pasaporte'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'nacionalidad'); ?>
		<?php echo $form->textField($model, 'nacionalidad', array('maxlength' => 25)); ?>
		<?php echo $form->error($model,'nacionalidad'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'cantidaddeadultos'); ?>
		<?php echo $form->textField($model, 'cantidaddeadultos'); ?>
		<?php echo $form->error($model,'cantidaddeadultos'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'cantidaddeninos'); ?>
		<?php echo $form->textField($model, 'cantidaddeninos'); ?>
		<?php echo $form->error($model,'cantidaddeninos'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'Escala'); ?>
		<?php echo $form->checkBox($model, 'cantidaddeescalas'); ?>
		<?php echo $form->error($model,'cantidaddeescalas'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'numerovuelo'); ?>
		<?php echo $form->dropDownList($model, 'numerovuelo', GxHtml::listDataEx(Vuelos::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'numerovuelo'); ?>
		</div><!-- row -->
</div>


<?php
echo GxHtml::submitButton(Yii::t('app', 'Comprar'), array('maxlength' => 25));
$this->endWidget();
?>
</div><!-- form -->