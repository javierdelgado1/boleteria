<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pdf.css" />
<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);

?>

<h1><?php echo Yii::t('app', 'Boleto -') . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php

$vuelo = Vuelos::model()->findByPk($model->numerovuelo);
$this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
'idcompra',
'nombre',
'apellido',
'pasaporte',
'nacionalidad',
'cantidaddeadultos',
'cantidaddeninos',
'cantidaddeescalas',

array(
			'name' => 'numerovuelo0',
			'type' => 'raw',
			'value' => $model->numerovuelo0 !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->numerovuelo0)), array('vuelos/view', 'id' => GxActiveRecord::extractPkValue($model->numerovuelo0, true))) : null,
			),
array(
			'name' => 'Origen',
			'type' => 'raw',
			'value' => $vuelo->idorigen0 !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($vuelo->idorigen0)), array('origenes/view', 'id' => GxActiveRecord::extractPkValue($vuelo->idorigen0, true))) : null,
			),
array(
			'name' => 'Destino',
			'type' => 'raw',
			'value' => $vuelo->iddestino0 !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($vuelo->iddestino0)), array('destinos/view', 'id' => GxActiveRecord::extractPkValue($vuelo->iddestino0, true))) : null,
			),
array(
			'name' => 'Costo total Bs',
			'type' => 'raw',
			'value' => $vuelo->costo !== null ? $model->cantidaddeninos !== null ? $vuelo->costo*$model->cantidaddeadultos + ($vuelo->costo/2)*$model->cantidaddeninos : $vuelo->costo*$model->cantidaddeadultos : null,
			),
array(
			'name' => 'Costo por adulto Bs',
			'type' => 'raw',
			'value' => $vuelo->costo !== null ? $vuelo->costo: null,
			),
array(
			'name' => 'Costo por ni&ntilde;o Bs',
			'type' => 'raw',
			'value' => $vuelo->costo !== null ? $vuelo->costo/2: null,
			),
	),

)); ?>

