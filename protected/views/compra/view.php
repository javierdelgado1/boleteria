<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);

$this->menu=array(
	array('label'=>Yii::t('app', 'Listar') . ' ' . $model->label(2), 'url'=>array('index')),
	array('label'=>Yii::t('app', 'Crear') . ' ' . $model->label(), 'url'=>array('create')),
	array('label'=>Yii::t('app', 'Actualizar') . ' ' . $model->label(), 'url'=>array('update', 'id' => $model->idcompra)),
	array('label'=>Yii::t('app', 'Cancelar') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->idcompra), 'confirm'=>'¿Seguro que desea eliminar esta compra?')),
	array('label'=>Yii::t('app', 'Gestionar') . ' ' . $model->label(2), 'url'=>array('admin')),
	array('label'=>Yii::t('app', 'Generar Factura PDF') . ' ' . $model->label(), 'url'=>array('pdf', 'id' => $model->idcompra), 'linkOptions' => array('target'=>'_blank')),
);
?>

<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php 
$vuelo = Vuelos::model()->findByPk($model->numerovuelo);
$costoTotal = null;
$descuentoEscala = 0;
if($vuelo->costo !== null){
	if($model->cantidaddeadultos !== null)
		$costoTotal = $vuelo->costo*$model->cantidaddeadultos;
	if($model->cantidaddeninos !== null)
		$costoTotal = $costoTotal + ($vuelo->costo/2)*$model->cantidaddeninos;
	if($model->cantidaddeescalas == "1"){
		$descuentoEscala = $costoTotal*(.2);
		$costoTotal = $costoTotal - $descuentoEscala;
	}
}


$this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
'idcompra',
'nombre',
'apellido',
'pasaporte',
'nacionalidad',
'cantidaddeadultos',
'cantidaddeninos',
'cantidaddeescalas',
array(
			'name' => 'numerovuelo0',
			'type' => 'raw',
			'value' => $model->numerovuelo0 !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->numerovuelo0)), array('vuelos/view', 'id' => GxActiveRecord::extractPkValue($model->numerovuelo0, true))) : null,
			),
array(
			'name' => 'Origen',
			'type' => 'raw',
			'value' => $vuelo->idorigen0 !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($vuelo->idorigen0)), array('origenes/view', 'id' => GxActiveRecord::extractPkValue($vuelo->idorigen0, true))) : null,
			),
array(
			'name' => 'Destino',
			'type' => 'raw',
			'value' => $vuelo->iddestino0 !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($vuelo->iddestino0)), array('destinos/view', 'id' => GxActiveRecord::extractPkValue($vuelo->iddestino0, true))) : null,
			),
array(
			'name' => 'Costo total Bs',
			'type' => 'raw',
			'value' => $costoTotal !== null ? $costoTotal : null,
			),
array(
			'name' => 'Descuento escala',
			'type' => 'raw',
			'value' => $descuentoEscala !== null ? $descuentoEscala : null,
			),
array(
			'name' => 'Costo por adulto Bs',
			'type' => 'raw',
			'value' => $vuelo->costo !== null ? $vuelo->costo: null,
			),
array(
			'name' => 'Costo por ni&ntilde;o Bs',
			'type' => 'raw',
			'value' => $vuelo->costo !== null ? $vuelo->costo/2: null,
			),
	),
)); ?>

