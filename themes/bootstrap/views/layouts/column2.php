<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>

  
        <div   >
            <?php echo $content; ?>
        </div><!-- content -->
    
   
        <div id="sidebar">
        <?php
            $this->beginWidget('zii.widgets.CPortlet', array(
                'title'=>'Opciones',
            ));
            $this->widget('bootstrap.widgets.TbMenu', array(
                'items'=>$this->menu,
                'htmlOptions'=>array('class'=>'operations'),
            ));
            $this->endWidget();
        ?>
        </div><!-- sidebar -->
  

<?php $this->endContent(); ?>