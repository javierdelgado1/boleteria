<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="es" />
	<link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/favicon.png" type="image/x-icon" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/styles.css" />

	<title>Sistema de Venta de Boletos</title>

	<?php Yii::app()->bootstrap->register(); ?>
</head>

<body>
<div class="navbar navbar-fixed-top">
	<div class="navbar-inner">
			<div class ="container">
				<button type="button" class="btn btn-navbar" data-toogle="collapse" data-target=".nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="brand" href="<?php echo Yii::app()->homeURL; ?>">
					Sistema de Venta de Boletos
				</a>

				<div class="nav-collapse collapse">
					<?php $this->widget('zii.widgets.CMenu',array(
					'items'=>array(	

						array('label'=>'Viajar', 'url'=>array('/compra/create'), 'visible'=>Yii::app()->user->isGuest),
						array('label'=>'Vuelos', 'url'=>array('/vuelos/index'), 'visible'=>Yii::app()->user->isGuest),		
						array('label'=>'Panel', 'url'=>array('/PanelAdministrador'), 'visible'=>!Yii::app()->user->isGuest),					
						array('label'=>'Administrador', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
						array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
						),
					'htmlOptions' => array('class' => 'nav navbar-nav'),
					)); ?>
				</div>
			</div>	
	</div>
</div><!-- mainmenu -->

<div class="container">
	<div class="page-header">
		<br> </br>
		<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
			)); ?><!-- breadcrumbs -->
		<?php endif?>
	</div>

	<div class="well">
		<?php echo $content; ?>


	</div>



</div>

<div class="footer text-center">
	Copyright &copy; <?php echo date('Y'); ?> by Ricardo Felicce, Javier Delgado y Osman Villegas.<br/>
	Todos los derechos reservados.<br/>
	<?php echo Yii::powered(); ?>
</div><!-- footer -->
</body>
</html>
