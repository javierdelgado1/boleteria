<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Autentificacion';
$this->breadcrumbs=array(
	'Autentificacion',
);
?>

<h1>Autentificacion</h1>


<div class="alert alert-info">
 	Por favor complete el siguiente formulario con sus datos de acceso:
</div>

<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'login-form',
    'type'=>'horizontal',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<p class="note">Rellene todos los campos que digan <span class="required">*</span> </p>

	<?php echo $form->textFieldRow($model,'username'); ?>

	<?php echo $form->passwordFieldRow($model,'password'); ?>

	<?php echo $form->checkBoxRow($model,'rememberMe'); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>'Iniciar sesion',
        )); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
