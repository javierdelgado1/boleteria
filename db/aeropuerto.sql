-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 12-06-2014 a las 02:09:29
-- Versión del servidor: 5.6.16
-- Versión de PHP: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `aeropuerto`
--
CREATE DATABASE IF NOT EXISTS `aeropuerto` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
-- --------------------------------------------------------
use `aeropuerto`;
--
-- Estructura de tabla para la tabla `administradores`
--

CREATE TABLE IF NOT EXISTS `administradores` (
  `idadministrador` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(25) NOT NULL,
  `contrasena` varchar(25) NOT NULL,
  PRIMARY KEY (`idadministrador`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `administradores`
--

INSERT INTO `administradores` (`idadministrador`, `usuario`, `contrasena`) VALUES
(1, 'Ricardo', '123'),
(2, 'admin', 'admin'),
(3, 'cheche', '123');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aerolineas`
--

CREATE TABLE IF NOT EXISTS `aerolineas` (
  `idaerolinea` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(25) NOT NULL,
  `rif` varchar(25) NOT NULL,
  PRIMARY KEY (`idaerolinea`),
  UNIQUE KEY `nombre` (`nombre`),
  UNIQUE KEY `rif` (`rif`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=76 ;

--
-- Volcado de datos para la tabla `aerolineas`
--

INSERT INTO `aerolineas` (`idaerolinea`, `nombre`, `rif`) VALUES
(1, 'Conviasa', 'j123456789'),
(4, 'Air Argentina', 'J-111111235'),
(5, 'Air Colombia', 'J-84564452'),
(6, 'Air Usa', 'G-0000002'),
(7, 'a', 'a');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `boletos`
--

CREATE TABLE IF NOT EXISTS `boletos` (
  `idboleto` int(11) NOT NULL AUTO_INCREMENT,
  `idvuelo` int(11) NOT NULL,
  `idpasajero` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `estado` tinyint(1) NOT NULL,
  PRIMARY KEY (`idboleto`),
  KEY `idvuelo` (`idvuelo`),
  KEY `idpasajero` (`idpasajero`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clases`
--

CREATE TABLE IF NOT EXISTS `clases` (
  `idclase` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(25) NOT NULL,
  PRIMARY KEY (`idclase`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `clases`
--

INSERT INTO `clases` (`idclase`, `nombre`) VALUES
(1, 'Turista'),
(2, 'Ejecutiva/Business'),
(3, 'Primera Clase');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compra`
--

CREATE TABLE IF NOT EXISTS `compra` (
  `idcompra` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(25) NOT NULL,
  `apellido` varchar(25) NOT NULL,
  `pasaporte` varchar(25) NOT NULL,
  `nacionalidad` varchar(25) NOT NULL,
  `cantidaddeadultos` int(11) NOT NULL,
  `cantidaddeninos` int(11) NOT NULL,
  `cantidaddeescalas` tinyint(1) NOT NULL,
  `numerovuelo` int(11) NOT NULL,
  PRIMARY KEY (`idcompra`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Volcado de datos para la tabla `compra`
--

INSERT INTO `compra` (`idcompra`, `nombre`, `apellido`, `pasaporte`, `nacionalidad`, `cantidaddeadultos`, `cantidaddeninos`, `cantidaddeescalas`, `numerovuelo`) VALUES
(12, '-javer', 'Delgado', 'sdfsdfsdf', 'kjhjkhksdf', 1, 1, 1, 1),
(15, '-javer', 'Delgado', 'sdfsdfsdf', 'FGDGDFG', 1, 1, 1, 1),
(16, '-javer', 'Delgado', 'sdfsdfsdf', 'sdfsdf', 1, 1, 1, 12),
(17, '-javer', 'Delgado', 'DFSDFSDF', 'adsadsad', 1, 1, 1, 12),
(18, 'sdfdsf', 'sdfsdf', 'sdfsd', 'sdfdsf', 2, 1, 4, 1),
(19, 'a', 'a', 'a', 'a', 1, 1, 1, 1),
(20, 'b', 'b', 'b', 'b', 1, 1, 1, 1),
(21, 'c', 'c', 'c', 'c', 1, 1, 1, 1),
(22, 'd', 'd', 'd', 'd', 3, 3, 3, 1),
(23, 'f', 'f', 'f', 'f', 1, 1, 1, 1),
(24, 'd', 'd', 'r', 'e', 1, 1, 0, 15),
(25, 'd', 'd', 'r', 'e', 1, 1, 0, 15),
(26, 'd', 'd', 'r', 'e', 1, 1, 0, 15);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `destinos`
--

CREATE TABLE IF NOT EXISTS `destinos` (
  `iddestino` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(25) NOT NULL,
  PRIMARY KEY (`iddestino`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `destinos`
--

INSERT INTO `destinos` (`iddestino`, `nombre`) VALUES
(1, 'Distrito Capital'),
(2, 'Buenos Aires'),
(3, 'Miami'),
(4, 'Bogota');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `origenes`
--

CREATE TABLE IF NOT EXISTS `origenes` (
  `idorigen` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(25) NOT NULL,
  PRIMARY KEY (`idorigen`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Volcado de datos para la tabla `origenes`
--

INSERT INTO `origenes` (`idorigen`, `nombre`) VALUES
(1, 'Amazonas'),
(2, 'Anzoategui'),
(3, 'Apure'),
(4, 'Aragua'),
(5, 'Barinas'),
(6, 'Bolivar'),
(7, 'Carabobo'),
(8, 'Cojedes'),
(9, 'Delta Amacuro'),
(10, 'Distrito Capital'),
(11, 'Falcon'),
(12, 'Guarico'),
(13, 'Lara'),
(14, 'Merida'),
(15, 'Miranda'),
(16, 'Monagas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pasajeros`
--

CREATE TABLE IF NOT EXISTS `pasajeros` (
  `idpasajeros` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(25) NOT NULL,
  `apellido` varchar(25) NOT NULL,
  `pasaporte` varchar(25) NOT NULL,
  `nacionalidad` varchar(25) NOT NULL,
  PRIMARY KEY (`idpasajeros`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `pasajeros`
--

INSERT INTO `pasajeros` (`idpasajeros`, `nombre`, `apellido`, `pasaporte`, `nacionalidad`) VALUES
(1, 'Javier', 'Delgado', '12312312312', 'Venezolano');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vuelohoras`
--

CREATE TABLE IF NOT EXISTS `vuelohoras` (
  `idHora` int(11) NOT NULL AUTO_INCREMENT,
  `hora` varchar(8) NOT NULL,
  PRIMARY KEY (`idHora`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vuelos`
--

CREATE TABLE IF NOT EXISTS `vuelos` (
  `idvuelo` int(11) NOT NULL AUTO_INCREMENT,
  `idaerolinea` int(11) NOT NULL,
  `hora` time NOT NULL,
  `idorigen` int(11) NOT NULL,
  `iddestino` int(11) NOT NULL,
  `idclase` int(11) NOT NULL,
  `costo` float NOT NULL,
  `maxboleto` int(11) NOT NULL,
  PRIMARY KEY (`idvuelo`),
  UNIQUE KEY `idaerolinea_2` (`idaerolinea`,`hora`,`idorigen`,`iddestino`),
  KEY `idaerolinea` (`idaerolinea`),
  KEY `idorigen` (`idorigen`),
  KEY `iddestino` (`iddestino`),
  KEY `idclase` (`idclase`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Volcado de datos para la tabla `vuelos`
--

INSERT INTO `vuelos` (`idvuelo`, `idaerolinea`, `hora`, `idorigen`, `iddestino`, `idclase`, `costo`, `maxboleto`) VALUES
(1, 1, '07:00:00', 1, 1, 1, 1300, 100),
(2, 1, '01:00:00', 14, 1, 3, 2500, 20),
(11, 1, '04:00:00', 1, 1, 1, 650, 123),
(12, 7, '00:00:03', 1, 1, 1, 800, 123),
(14, 7, '00:00:03', 1, 2, 1, 1200, 321),
(15, 7, '00:00:07', 1, 2, 1, 1200, 14);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `boletos`
--
ALTER TABLE `boletos`
  ADD CONSTRAINT `boletos_ibfk_1` FOREIGN KEY (`idvuelo`) REFERENCES `vuelos` (`idvuelo`),
  ADD CONSTRAINT `boletos_ibfk_2` FOREIGN KEY (`idpasajero`) REFERENCES `pasajeros` (`idpasajeros`);

--
-- Filtros para la tabla `vuelos`
--
ALTER TABLE `vuelos`
  ADD CONSTRAINT `vuelos_ibfk_1` FOREIGN KEY (`idaerolinea`) REFERENCES `aerolineas` (`idaerolinea`),
  ADD CONSTRAINT `vuelos_ibfk_2` FOREIGN KEY (`idorigen`) REFERENCES `origenes` (`idorigen`),
  ADD CONSTRAINT `vuelos_ibfk_3` FOREIGN KEY (`iddestino`) REFERENCES `destinos` (`iddestino`),
  ADD CONSTRAINT `vuelos_ibfk_4` FOREIGN KEY (`idclase`) REFERENCES `clases` (`idclase`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
